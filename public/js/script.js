function addNum() {
    var firstNum=parseInt(document.getElementById('num1').value);
    var secondNum= parseInt(document.getElementById('num2').value);
    var result = firstNum + secondNum;

    document.getElementById('result').innerHTML= result;
    return false;
}

function subNum() {
    var firstNum=parseInt(document.getElementById('num1').value);
    var secondNum= parseInt(document.getElementById('num2').value);
    var result = firstNum - secondNum;

    document.getElementById('result').innerHTML= result;
    return false;
}

function multiplyNum() {
    var firstNum=parseInt(document.getElementById('num1').value);
    var secondNum= parseInt(document.getElementById('num2').value);
    var result = firstNum * secondNum;

    document.getElementById('result').innerHTML= result;
    return false;
}

function divideNum() {
    var firstNum=parseInt(document.getElementById('num1').value);
    var secondNum= parseInt(document.getElementById('num2').value);
    var result = firstNum / secondNum;

    document.getElementById('result').innerHTML= result;
    return false;
    
}


 function resetForm() {
    document.getElementById("myForm").reset();
    multiplyNum().reset();
    subNum().reset();
    addNum().reset();
    divideNum().reset();
    
    
}